const db = firebase.firestore();

const PedidoForm = document.getElementById("pedido-form");
const PedidosContainer = document.getElementById("pedidos-container");

let editStatus = false;
let id = '';

/**
 * Save a New Pedido in Firestore
 * @param {string} id_pedido the id_pedido of the Pedido
 * @param {string} state_type_id the state_type_id of the Pedido
 */
const savePedido = (id_pedido, state_type_id) =>
  db.collection("pedidos").doc().set({
    id_pedido,
    state_type_id,
  });

const getPedidos = () => db.collection("pedidos").get();

const onGetPedidos = (callback) => db.collection("pedidos").onSnapshot(callback);

const deletePedido = (id) => db.collection("pedidos").doc(id).delete();

const getPedido = (id) => db.collection("pedidos").doc(id).get();

const updatePedido = (id, updatedPedido) => db.collection('pedidos').doc(id).update(updatePedido);

window.addEventListener("DOMContentLoaded", async (e) => {
  onGetPedidos((querySnapshot) => {
    PedidosContainer.innerHTML = "";

    querySnapshot.forEach((doc) => {
      const pedido = doc.data();

      PedidosContainer.innerHTML += `<div class="card card-body mt-2 border-primary">
    <h3 class="h5">${pedido.id_pedido}</h3>
    <p>${pedido.state_type_id}</p>
    <div>
      <button class="btn btn-primary btn-delete" data-id="${doc.id}">
        🗑 Delete
      </button>
      <button class="btn btn-secondary btn-edit" data-id="${doc.id}">
        🖉 Edit
      </button>
    </div>
  </div>`;
    });

    const btnsDelete = PedidosContainer.querySelectorAll(".btn-delete");
    btnsDelete.forEach((btn) =>
      btn.addEventListener("click", async (e) => {
        console.log(e.target.dataset.id);
        try {
          await deletePedido(e.target.dataset.id);
        } catch (error) {
          console.log(error);
        }
      })
    );

    const btnsEdit = PedidosContainer.querySelectorAll(".btn-edit");
    btnsEdit.forEach((btn) => {
      btn.addEventListener("click", async (e) => {
        try {
          const doc = await getPedido(e.target.dataset.id);
          const pedido = doc.data();
          PedidoForm["pedido-id_pedido"].value = pedido.id_pedido;
          PedidoForm["pedido-state_type_id"].value = pedido.state_type_id;

          editStatus = true;
          id = doc.id;
          PedidoForm["btn-pedido-form"].innerText = "Update";

        } catch (error) {
          console.log(error);
        }
      });
    });
  });
});

PedidoForm.addEventListener("submit", async (e) => {
  e.preventDefault();

  const id_pedido = PedidoForm["pedido-id_pedido"];
  const state_type_id = PedidoForm["pedido-state_type_id"];

  try {
    if (!editStatus) {
      await savePedido(id_pedido.value, state_type_id.value);
    } else {
      await updatePedido(id, {
        id_pedido: id_pedido.value,
        state_type_id: state_type_id.value,
      })

      editStatus = false;
      id = '';
      PedidoForm['btn-pedido-form'].innerText = 'Save';
    }

    PedidoForm.reset();
    id_pedido.focus();
  } catch (error) {
    console.log(error);
  }
});